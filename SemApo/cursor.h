#ifndef __CURSOR_H__
#define __CURSOR_H__

#include <iostream>
#include <vector>
#include <string>

#include "support.h"


using namespace std;

class Menu;
class Submenu;

class Cursor {
public:
	Cursor(int xCursorOffset, int yCursorOffset, Menu* menu);
	Cursor(int xCursorOffset, int yCursorOffset, Submenu* menu);
	~Cursor();

	/**
	* Repeatedly called method update,
	* updating cursor: highlighting current row
	*/
	void update(); //TODO

	/**
	* Repeatedly called method draw,
	* draw cursor
	*/
	void draw(uint16_t* paintingBuffer); //TODO

	string toString();

	// SETTERS
	void setMaxRow(int* maxRow);

	void increaseMaxRow(int i); // maybe redundant
	void moveDown();
	void moveUp();
	void setCurRow(int idx);

	int getCurRow();


private:
	Menu* menu;
	int xCursorOffset;
	int yCursorOffset;
	int yDownCursorOffset;

	int xPos;
	int yPos;

	int* curMinRow; // current number of min viewed row of Unit
	int* curMaxRow; // current number of max viewed row of Unit

	int curRow;
	int* maxRow;

	int scale;

	int maxRowInWin;


};





#endif
