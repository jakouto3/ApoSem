#include "network.h"


void statusMessageSender(Unit* theUnit) {
	while (true) {
		sendStatusMessage(theUnit);
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}
}

void sendStatusMessage(Unit* theUnit) {
	int socketFileDescriptor;
	struct sockaddr_in broadcast_address;
	memset(&broadcast_address, 0, sizeof(struct sockaddr_in));
	int yes = 1;

	socketFileDescriptor = socket(AF_INET, SOCK_DGRAM, 0);
	if (socketFileDescriptor == -1) {
		perror("couldn't create socket");
		exit(1);
	}
	if(setsockopt(socketFileDescriptor, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) {
		perror("setsockopt SO_REUSEADDR fail");
		exit(1);
	}
	if (setsockopt(socketFileDescriptor, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes)) == -1) {
		perror("setsockopt SO_BROADCAST fail");
		exit(1);
	}

	broadcast_address.sin_family = AF_INET;
	broadcast_address.sin_port = htons(COMMUNICATION_PORT);
	broadcast_address.sin_addr.s_addr = INADDR_BROADCAST;

	char statusMessage[MESSAGE_SIZE_STATUS] = {0};
	createStatusMessage(statusMessage, theUnit);

	sendto(socketFileDescriptor, statusMessage, sizeof(statusMessage), 0, (struct sockaddr *) &broadcast_address, sizeof(struct sockaddr_in));
	
	theUnit->setTOLCounter(theUnit->getTOLCounter()+1);

	close(socketFileDescriptor);
}

void messageReceiver(Menu* theMenu) {
	// ############ PREPARE NETWORK CODE ############
	int socketFileDescriptor;
	struct sockaddr_in bindAddress, fromAddress;
	socklen_t fromSize = sizeof(fromAddress);
	memset(&bindAddress, 0, sizeof(struct sockaddr_in)); //clear the structures
	memset(&fromAddress, 0, sizeof(struct sockaddr_in));
	int yes = 1;

	socketFileDescriptor = socket(AF_INET, SOCK_DGRAM, 0);
	if (socketFileDescriptor == -1) {
		perror("couldn't create socket");
		exit(1);
	}

	if (setsockopt(socketFileDescriptor, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) {
		perror("setsockopt SO_REUSEADDR fail for receiver");
		exit(1);
	}

	bindAddress.sin_family = AF_INET;
	bindAddress.sin_port = htons(COMMUNICATION_PORT);
	bindAddress.sin_addr.s_addr = INADDR_ANY;

	if (bind(socketFileDescriptor, (struct sockaddr*)&bindAddress, sizeof(bindAddress)) == -1) {
		perror("bind error");
		exit(1);
	}
	// ############ PREPARE RECEIVE BUFFER ############
	char receiveBuffer[MESSAGE_SIZE_STATUS]; //the status message is the largest

	// ############ RECEIVING LOOP ############
	while (true) {
		memset(receiveBuffer, 0, sizeof(receiveBuffer)); //clear the receive buffer
		int receivedBytes = recvfrom(socketFileDescriptor, receiveBuffer, sizeof(receiveBuffer), 0, (struct sockaddr*)&fromAddress, &fromSize);
		if (receivedBytes >= MESSAGE_SIZE_COMMAND && isReceivedMessageValidALCMessage(receiveBuffer)) {
			switch (getMessageType(receiveBuffer)) {
			case MESSAGE_TYPE_STATUS:
				handleType0Message(receiveBuffer, fromAddress.sin_addr.s_addr, theMenu);
				break;
			case MESSAGE_TYPE_INCREMENT:
				handleType1Message(receiveBuffer, theMenu->getUnitVector()[0]); //getting the first unit in unit list (e.g. this unit)
				break;
			case MESSAGE_TYPE_SET:
				handleType2Message(receiveBuffer, theMenu->getUnitVector()[0]); //getting the first unit in unit list (e.g. this unit)
				break;
			default:
				break;
			}
		}
	}
}

void handleType0Message(char* messageArray, uint32_t ipaddress, Menu* theMenu) {
	vector<Unit*>& units = theMenu->getUnitVector();
	int oldestUnitIdx = -1;
	int foundUnitIdx = -1;
	bool unitInVector = false;
	for (uint32_t unitIdx = 0; unitIdx < units.size(); ++unitIdx) {
		if (units[unitIdx]->getIPAddress() == ipaddress) {
			foundUnitIdx = unitIdx;
			unitInVector = true;
		}
		if (units[0]->getTOLCounter() - units[unitIdx]->getTOLCounter() > 20) {
			oldestUnitIdx = unitIdx;
		}
			
	}
	
	if(oldestUnitIdx > 0) {
		theMenu->removeUnit(oldestUnitIdx);
	}

	if (unitInVector) {
		updateUnitFromStatusMessage(messageArray, units[foundUnitIdx]);
	} else if (getNameFromStatusMessage(messageArray) != units[0]->getName()) {
		Unit* newUnit = createNewUnitFromMessage(messageArray, ipaddress, theMenu);
		theMenu->addUnit(newUnit);
	}
}


void handleType1Message(char* messageArray, Unit* theUnit) {
	int16_t ceilingRedChange, ceilingGreenChange, ceilingBlueChange, wallsRedChange, wallsGreenChange, wallsBlueChange;
	RGB888_t origCeilingColor = theUnit->getCeiling();
	RGB888_t origWallsColor = theUnit->getWalls();
	RGB888_t newCeilingColor, newWallsColor;

	memcpy(&ceilingRedChange, messageArray + MESSAGE_HEADER_SIZE, sizeof(int16_t));
	memcpy(&ceilingGreenChange, messageArray + MESSAGE_HEADER_SIZE + sizeof(int16_t), sizeof(int16_t));
	memcpy(&ceilingBlueChange, messageArray + MESSAGE_HEADER_SIZE + 2 * sizeof(int16_t), sizeof(int16_t));

	memcpy(&wallsRedChange, messageArray + MESSAGE_HEADER_SIZE + 3 * sizeof(int16_t), sizeof(int16_t));
	memcpy(&wallsGreenChange, messageArray + MESSAGE_HEADER_SIZE + 4 * sizeof(int16_t), sizeof(int16_t));
	memcpy(&wallsBlueChange, messageArray + MESSAGE_HEADER_SIZE + 5 * sizeof(int16_t), sizeof(int16_t));

	ceilingRedChange = ntohs(ceilingRedChange);
	ceilingGreenChange = ntohs(ceilingGreenChange);
	ceilingBlueChange = ntohs(ceilingBlueChange);

	wallsRedChange = ntohs(wallsRedChange);
	wallsGreenChange = ntohs(wallsGreenChange);
	wallsBlueChange = ntohs(wallsBlueChange);

	newCeilingColor.red = getNewColorIntensity(origCeilingColor.red, ceilingRedChange);
	newCeilingColor.green = getNewColorIntensity(origCeilingColor.green, ceilingGreenChange);
	newCeilingColor.blue = getNewColorIntensity(origCeilingColor.blue, ceilingBlueChange);

	newWallsColor.red = getNewColorIntensity(origWallsColor.red, wallsRedChange);
	newWallsColor.green = getNewColorIntensity(origWallsColor.green, wallsGreenChange);
	newWallsColor.blue = getNewColorIntensity(origWallsColor.blue, wallsBlueChange);

	theUnit->setCeiling(newCeilingColor);
	theUnit->setWalls(newWallsColor);
}

void handleType2Message(char* messageArray, Unit* theUnit) {
	int16_t ceilingNewRed, ceilingNewGreen, ceilingNewBlue, wallsNewRed, wallsNewGreen, wallsNewBlue;
	RGB888_t origCeilingColor = theUnit->getCeiling();
	RGB888_t origWallsColor = theUnit->getWalls();
	RGB888_t newCeilingColor, newWallsColor;

	memcpy(&ceilingNewRed, messageArray + MESSAGE_HEADER_SIZE, sizeof(int16_t));
	memcpy(&ceilingNewGreen, messageArray + MESSAGE_HEADER_SIZE + sizeof(int16_t), sizeof(int16_t));
	memcpy(&ceilingNewBlue, messageArray + MESSAGE_HEADER_SIZE + 2 * sizeof(int16_t), sizeof(int16_t));

	memcpy(&wallsNewRed, messageArray + MESSAGE_HEADER_SIZE + 3 * sizeof(int16_t), sizeof(int16_t));
	memcpy(&wallsNewGreen, messageArray + MESSAGE_HEADER_SIZE + 4 * sizeof(int16_t), sizeof(int16_t));
	memcpy(&wallsNewBlue, messageArray + MESSAGE_HEADER_SIZE + 5 * sizeof(int16_t), sizeof(int16_t));

	ceilingNewRed = ntohs(ceilingNewRed);
	ceilingNewGreen = ntohs(ceilingNewGreen);
	ceilingNewBlue = ntohs(ceilingNewBlue);

	wallsNewRed = ntohs(wallsNewRed);
	wallsNewGreen = ntohs(wallsNewGreen);
	wallsNewBlue = ntohs(wallsNewBlue);

	newCeilingColor.red = (ceilingNewRed < 0) ? origCeilingColor.red : ceilingNewRed;
	newCeilingColor.green = (ceilingNewGreen < 0) ? origCeilingColor.green : ceilingNewGreen;
	newCeilingColor.blue = (ceilingNewBlue < 0) ? origCeilingColor.blue : ceilingNewBlue;

	newWallsColor.red = (wallsNewRed < 0) ? origWallsColor.red : wallsNewRed;
	newWallsColor.green = (wallsNewGreen < 0) ? origWallsColor.green : wallsNewGreen;
	newWallsColor.blue = (wallsNewBlue < 0) ? origWallsColor.blue : wallsNewBlue;

	theUnit->setCeiling(newCeilingColor);
	theUnit->setWalls(newWallsColor);
}

void sendType2Message(Unit* theUnit){
	int socketFileDescriptor;
	struct sockaddr_in broadcast_address;
	memset(&broadcast_address, 0, sizeof(struct sockaddr_in));
	int yes = 1;

	socketFileDescriptor = socket(AF_INET, SOCK_DGRAM, 0);
	if (socketFileDescriptor == -1) {
		perror("couldn't create socket");
		exit(1);
	}
	if(setsockopt(socketFileDescriptor, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) {
		perror("setsockopt SO_REUSEADDR fail");
		exit(1);
	}

	broadcast_address.sin_family = AF_INET;
	broadcast_address.sin_port = htons(COMMUNICATION_PORT);
	broadcast_address.sin_addr.s_addr = theUnit->getIPAddress();

	char setMessage[MESSAGE_SIZE_COMMAND] = {0};
	createType2Message(setMessage, theUnit);
	
	cerr << "Network.sendType2Message: Message sent" << endl;
	sendto(socketFileDescriptor, setMessage, sizeof(setMessage), 0, (struct sockaddr *) &broadcast_address, sizeof(struct sockaddr_in));

	close(socketFileDescriptor);
}
