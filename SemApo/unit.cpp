//#define _CRT_SECURE_NO_WARNINGS
#include "unit.h"
#include "menu.h"


Unit::Unit(string name, RGB888_t ceiling, RGB888_t walls, uint16_t *image, Menu* menu) {
	this->name = name;
	copy(name.begin(), name.end() - 1, this->bufferName);
	this->ceiling.store(ceiling);
	this->walls.store(walls);
	this->image = image;
	this->menu = menu;
	defaultScale = menu->getDefaultScale();
	scale = defaultScale;
	row = menu->getMaxRow();
	xPos = menu->getXMenuOffset();
	yPos = menu->getYMenuOffset() + ((2*row)%(2*menu->getMaxRowInWin()))*ROW_HEIGHT;
	
	local = false;
	inSubmenu = false;
	
	infoNewUnit();
}

Unit::Unit(string name, RGB888_t ceiling, RGB888_t walls, uint16_t *image, Menu* menu, bool local) {
	this->name = name;
	copy(name.begin(), name.end() - 1, this->bufferName);
	this->ceiling.store(ceiling);
	this->walls.store(walls);
	this->image = image;
	this->menu = menu;
	defaultScale = menu->getDefaultScale();
	scale = defaultScale;
	row = menu->getMaxRow();
	xPos = menu->getXMenuOffset();
	yPos = menu->getYMenuOffset() + ((2*row)%(2*menu->getMaxRowInWin()))*ROW_HEIGHT;
	
	this->local = local;
	
	inSubmenu = false;
	
	infoNewUnit();
}

/*
 * Destructor
 */
Unit::~Unit() {
	cerr << "Unit: " << name << " has been deleted" << endl;
	delete image;
}

// scelet methods

void Unit::update() {
	// xPos and yPos shouldn't change during executing program
	if (scale != defaultScale) scale = defaultScale;
	if(isLocal()){
		unsigned char *mem_base = (unsigned char*)map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
		*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = RGB888_tToUint32_t(this->getWalls());
		*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = RGB888_tToUint32_t(this->getCeiling());
	}
}

void Unit::draw(uint16_t* paintingBuffer) {
	// drawing on position xPos, yPos
	drawImg(paintingBuffer, image, 16, 16, xPos, yPos + 10);
	drawString(name, COLOR_BLACK, xPos + 20, yPos, paintingBuffer, scale);
}

void Unit::updateCoorToDrawFromRowPos(){
		xPos = menu->getXMenuOffset();
	yPos = menu->getYMenuOffset() + ((2*row)%(2*menu->getMaxRowInWin()))*ROW_HEIGHT;
	}

void Unit::infoNewUnit() {
	cerr << "Unit: New unit '" << name << "' (ceiling: " + to_string(ceiling.load().red) + " " + to_string(ceiling.load().green) + " " + to_string(ceiling.load().blue) + " walls: "
		+ to_string(walls.load().red) + " " + to_string(walls.load().green) + " " + to_string(walls.load().blue) + ") has been created..." << endl;
}

string Unit::toString() {
	return "Unit '" + name + "' (ceiling: " + to_string(ceiling.load().red) + " " + to_string(ceiling.load().green) + " " + to_string(ceiling.load().blue) + " walls: "
		+ to_string(walls.load().red) + " " + to_string(walls.load().green) + " " + to_string(walls.load().blue) + ") row: " + to_string(row) +
		" [x,y] = [" + to_string(xPos) + "][" + to_string(yPos) + "]";
}




// Setters

void Unit::setName(string name) {
	this->name = name;
	copy(name.begin(), name.end() - 1, this->bufferName);
}
void Unit::setWallsCeiling(RGB888_t walls, RGB888_t ceiling) {
	this->setWalls(walls);
	this->setCeiling(ceiling);
}
void Unit::setWalls(RGB888_t walls) {
	this->walls.store(walls);
}
void Unit::setCeiling(RGB888_t Ceiling) {
	this->ceiling.store(Ceiling);
}
void Unit::setImage(uint16_t *image) {
	this->image = image;
}

void Unit::setIPAddress(uint32_t ipaddress) {
	this->IPAddress.store(ipaddress);
}

void Unit::setXYPos(int x, int y) {
	xPos = x;
	yPos = y;
}

void Unit::setRow(int row) {
	this->row = row;
}

void Unit::setScale(int scale) {
	this->scale = scale;
}

void Unit::setIsInSubmenu(bool inSubmenu){
		this->inSubmenu = inSubmenu;
	}
	
void Unit::setTOLCounter(uint32_t newCounter){
	this->timeOfLifeCounter.store(newCounter);
}


// Getters

char* Unit::getBufferName() {
	return bufferName;
}

string Unit::getName() {
	return name;
}

RGB888_t Unit::getCeiling() {
	return ceiling.load();
}

RGB888_t Unit::getWalls() {
	return walls.load();
}

uint16_t* Unit::getImage() {
	return image;
}

uint32_t Unit::getIPAddress() {
	return IPAddress.load();
}

int Unit::getXPos() {
	return xPos;
}

int Unit::getYPos() {
	return yPos;
}

int Unit::getRow() {
	return row;
}

bool Unit::isLocal(){
		return local;
}

bool Unit::isInSubmenu(){
		return inSubmenu;
	}
	
uint32_t Unit::getTOLCounter(){
	return timeOfLifeCounter.load();
}
/*
RGB888_t* Unit::getPWalls() {
	return &walls;
}

RGB888_t* Unit::getPCeiling() {
	return &ceiling;
}*/
