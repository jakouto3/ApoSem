
#include <iostream>

#include "test.h"
#include "unit.h"
#include "menu.h"
#include "knobs.h"

using namespace std;

void test() {
	Knobs* knobs = new Knobs();
	Menu *menu = new Menu(knobs, 20, 20, 5, 2);
	cout << "Filling menu" << endl;
	menu->addUnit(new Unit("Unit01", 1, 2, NULL, menu));
	menu->addUnit(new Unit("Unit02", 1, 2, NULL, menu));
	menu->addUnit(new Unit("Unit03", 1, 2, NULL, menu));
	menu->addUnit(new Unit("Unit04", 1, 2, NULL, menu));
	menu->addUnit(new Unit("Unit05", 1, 2, NULL, menu));
	menu->addUnit(new Unit("Unit06", 1, 2, NULL, menu));
	menu->addUnit(new Unit("Unit07", 1, 2, NULL, menu));
	menu->addUnit(new Unit("Unit08", 1, 2, NULL, menu));
	menu->addUnit(new Unit("Unit09", 1, 2, NULL, menu));
	menu->addUnit(new Unit("Unit10", 1, 2, NULL, menu));
	menu->addUnit(new Unit("Unit11", 1, 2, NULL, menu));
	menu->addUnit(new Unit("Unit12", 1, 2, NULL, menu));
	menu->addUnit(new Unit("Unit13", 1, 2, NULL, menu));
	cout << "________________________" << endl << endl;
	uint16_t * frame = new uint16_t[WIDTH*HEIGHT];
	while (true) {
		menu->update();
		menu->draw(frame);
	}
	cout << endl << "________________________" << endl;
	cout << menu->toString() << endl;

	cout << "Deleting" << endl;
	delete menu;
	delete frame;
	
	system("pause");
}