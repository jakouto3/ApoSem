#ifndef __NETWORK_UTILS_H__
#define __NETWORK_UTILS_H__

#include <cstdint>
#include <string>
#include <cstring>
#include <netinet/in.h>
#include "support.h"
#include "unit.h"
#include "menu.h"

#define COMMUNICATION_PORT 55555

using namespace std;

const uint32_t MESSAGE_TYPE_STATUS = 0x0;
const uint32_t MESSAGE_TYPE_INCREMENT = 0x1;
const uint32_t MESSAGE_TYPE_SET = 0x2;

const uint32_t MAGIC_NUMBER_ALC1 = 0x0 | (((uint8_t) 'A') << 24) | (((uint8_t) 'L') << 16) | (((uint8_t) 'C') << 8) | ((uint8_t) '1');
const uint32_t ALC_PROTOCOL_VERSION = 0x00010000;

const uint16_t MESSAGE_SIZE_STATUS = 548;
const uint8_t MESSAGE_SIZE_COMMAND = 24;
const uint8_t MESSAGE_HEADER_SIZE = 12;

void createMessageHeader(char* messageArray, uint32_t messageType);

void createStatusMessage(char* messageArray, Unit* theUnit);

uint32_t getMessageType(char* messageArray);

uint16_t* getIconFromMessage(char* messageArray);

bool isReceivedMessageValidALCMessage(char* messageArray);

Unit* createNewUnitFromMessage(char* messageArray, uint32_t ipaddress, Menu* theMenu);

void updateUnitFromStatusMessage(char* messageArray, Unit* theUnit);

string getNameFromStatusMessage(char* messageArray);

void createType2Message(char* messageArray, Unit* theUnit);

#endif

