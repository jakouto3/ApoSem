#ifndef __SUBMENU_H__
#define __SUBMENU_H__

#include <iostream>
#include <string>

#include "support.h"
#include "network.h"

using namespace std;

class Menu;
class Unit;
class Cursor;
class Knobs;


class Submenu {
public:
	Submenu(Knobs* knobs, Menu* menu, Unit* unit);
	~Submenu();

	void update();
	void draw(uint16_t* paintingBuffer);
	
	
	//GETTERS
	int* getPMaxRow();
	int getDefaultScale();
	int* getPCurMinRow();
	int* getPCurMaxRow();
	int getMaxRowInWin();

private:
	enum state {
		MENU, WALLS, CEILING, OFF_LIGHT, ON_LIGHTS
	};
	state curState;
	Menu* menu;
	Knobs* knobs;
	Cursor* cursor;
	Unit* unit;

	string name;

	int xSubmenuOffset;
	int ySubmenuOffset;
	
	RGB888_t walls;
	RGB888_t ceiling;
	string wallsNum;
	string ceilingNum;

	int maxRow;
	int defaultScale;
	int curMinRow;
	int curMaxRow;
	int maxRowInWin;

	int scaleTitle;
	int scaleWalls;
	int scaleCeiling;
	int scaleOffLight;
	int scaleOnLights;

	void highlightCurRow();

	void updateMenu();
	void updateWalls();
	void updateCeiling();
	void updateOffLights();
	void updateOnLights();

	void drawMenu(uint16_t* paintingBuffer);
	void drawOnLights(uint16_t* paintingBuffer);

	// check if green knob is pressed (true:yep, false:not)
	bool checkReturn();

	string toStringRGB888(RGB888_t value);

	// row and column position of cursor in turn on lights
	int rowPosCursor2; // y position
	int colPosCursor2; // x position
	int xPosCursor2; // x pos in pxs
	int yPosCursor2; // y pos in pxs
	int xCursor2Offset;
	int yCursor2Offset;

	// colors which can user choose
	uint16_t colorsChoice[16];

	void fillColorsChoice();

	//updating x/yposCursor2 according to row/colPosCursor2
	void updateXYPosCursor2();

};





#endif
