#include "submenu.h"
#include "menu.h"
#include "unit.h"
#include "cursor.h"
#include "knobs.h"


Submenu::Submenu(Knobs* knobs, Menu* menu, Unit* unit) {
	this->menu = menu;
	this->knobs = knobs;
	this->unit = unit;
	knobs->annulate();
	xSubmenuOffset = menu->getXMenuOffset();
	ySubmenuOffset = menu->getYMenuOffset();
	
	unit->setIsInSubmenu(true);
	
	maxRow = 4;
	scaleOnLights = scaleOffLight = scaleWalls = scaleCeiling = defaultScale = menu->getDefaultScale();
	scaleTitle = defaultScale + 1;
	curMinRow = 0;
	curMaxRow = 3;
	maxRowInWin = 4;
	name = unit->getName();

	//incrementingWalls = incrementingCeiling = false;

	//int offset = HEIGHT - ySubmenuOffset - 6 * ROW_HEIGHT;
	//if (offset < 0) ySubmenuOffset += offset;
	cursor = new Cursor(20, ySubmenuOffset + 2*ROW_HEIGHT, this);
	curState = MENU;

	fillColorsChoice();
	xCursor2Offset = xSubmenuOffset + 2 * ROW_HEIGHT;
	yCursor2Offset = ySubmenuOffset + 2 * ROW_HEIGHT;
	colPosCursor2 = rowPosCursor2 = 0;
	updateXYPosCursor2();
	
	cerr << "Submenu: submenu has been created" << endl;
}

Submenu::~Submenu() {
	knobs->annulate();
	delete cursor;
	cerr << "Submenu: submenu has been deleted" << endl;
}

void Submenu::update() {
	walls = unit->getWalls();
	ceiling = unit->getCeiling();

	//wallsNum = toStringRGB888(walls);
	//ceilingNum = toStringRGB888(ceiling);

	if (curState == MENU) {
		updateMenu();
		if (!menu->isSubmenuActive()) return;
	}
	else if (curState == WALLS) {
		updateWalls();
	}
	else if (curState == CEILING) {
		updateCeiling();
	}
	else if (curState == OFF_LIGHT) {
		updateOffLights();
	}
	else if (curState == ON_LIGHTS) {
		updateOnLights();
	}

	scaleOffLight = scaleOnLights = scaleWalls = scaleCeiling = defaultScale;
	cursor->update();
	highlightCurRow();

	wallsNum = toStringRGB888(walls);
	ceilingNum = toStringRGB888(ceiling);
	//cerr << "Ending update" << endl;
}

void Submenu::updateMenu() {
	// returning to menu (by pressing green knob)
	if (knobs->isGreenKnobPressed()) {
		menu->deactiveSubmenu();
		if(!unit->isLocal()) sendType2Message(unit);
		unit->setIsInSubmenu(false);
		return;
	}

	if (knobs->isBlueKnobPressed()) {
		if (cursor->getCurRow() == 0) {
			curState = WALLS;
			knobs->annulate();
			return;
		}
		else if (cursor->getCurRow() == 1) {
			curState = CEILING;
			knobs->annulate();
			return;
		}
		else if (cursor->getCurRow() == 2) {
			curState = OFF_LIGHT;
			knobs->annulate();
			return;
		}
		else if (cursor->getCurRow() == 3) {
			curState = ON_LIGHTS;
			knobs->annulate();
			return;
		}
		else cerr << "Submenu.update: Strength idx of cursor" << endl;
	}
	else if (knobs->isBlueTurnRight()) cursor->moveUp();
	else if (knobs->isBlueTurnLeft()) cursor->moveDown();
}

void Submenu::updateWalls() {
	// return in case green knob pressed
	if (checkReturn()) return;

	if (knobs->isRedTurnRight() && walls.red < 256) ++walls.red;
	else if (knobs->isRedTurnLeft() && walls.red > 0) --walls.red;

	if (knobs->isGreenTurnRight() && walls.green < 256) ++walls.green;
	else if (knobs->isGreenTurnLeft() && walls.green > 0) --walls.green;

	if (knobs->isBlueTurnRight() && walls.blue < 256) ++walls.blue;
	else if (knobs->isBlueTurnLeft() && walls.blue > 0) --walls.blue;
	unit->setWalls(walls);
}

void Submenu::updateCeiling() {
	// return in case green knob pressed
	if (checkReturn()) return;

	if (knobs->isRedTurnRight() && ceiling.red < 256) ++ceiling.red;
	else if (knobs->isRedTurnLeft() && ceiling.red > 0) --ceiling.red;

	if (knobs->isGreenTurnRight() && ceiling.green < 256) ++ceiling.green;
	else if (knobs->isGreenTurnLeft() && ceiling.green > 0) --ceiling.green;

	if (knobs->isBlueTurnRight() && ceiling.blue < 256) ++ceiling.blue;
	else if (knobs->isBlueTurnLeft() && ceiling.blue > 0) --ceiling.blue;
	unit->setCeiling(ceiling);
}

void Submenu::updateOffLights() {
	ceiling.red = ceiling.green = ceiling.blue = 0;
	walls.red = walls.green = walls.blue = 0;
	unit->setWallsCeiling(walls, ceiling);
	curState = MENU;
}

// change color for both walls and ceiling
void Submenu::updateOnLights() {
	if (checkReturn()) {
		rowPosCursor2 = colPosCursor2 = 0;
		return;
	}
	//cerr << "Im here updating onLights" << endl;
	if (knobs->isBlueTurnRight() && colPosCursor2 + 1 < 4) colPosCursor2++;
	else if (knobs->isBlueTurnLeft() && colPosCursor2 - 1 >= 0) colPosCursor2--;

	if (knobs->isGreenTurnLeft() && rowPosCursor2 + 1 < 4) rowPosCursor2++;
	else if (knobs->isGreenTurnRight() && rowPosCursor2 - 1 >= 0) rowPosCursor2--;
	updateXYPosCursor2();

	if (knobs->isBlueKnobPressed()) {
		int index = rowPosCursor2 * 4 + colPosCursor2;
		RGB888_t opt = uint16toRGB888(colorsChoice[index]);
		unit->setWallsCeiling(opt, opt);
	}

}

void Submenu::draw(uint16_t* paintingBuffer) {
	//cleaning screen of submenu
	cleanPaintingBuffer(paintingBuffer);
	
	if (curState == ON_LIGHTS) {
		drawOnLights(paintingBuffer);
		//cerr << "End of drawing onLights" << endl;
	}
	else {
		drawMenu(paintingBuffer);
		//cursor->draw(paintingBuffer); // SHOULD BE UNCOMMENTED
	}
	
}

void Submenu::drawOnLights(uint16_t* paintingBuffer) {
	//drawing title
	drawString("Choose color", COLOR_BLACK, xSubmenuOffset + 100, ySubmenuOffset,
		paintingBuffer, scaleTitle);
	//cerr << "In drawLights" << endl;
	//drawing mark/cursor
	fillRectangle(xPosCursor2 - 5, yPosCursor2 - 5, 32 + 10, COLOR_BLACK, paintingBuffer);
	
	//drawing colors rectangles
	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			fillRectangle(xCursor2Offset + j * 2 * ROW_HEIGHT,
				yCursor2Offset + i * 2 * ROW_HEIGHT, 32, colorsChoice[i * 4 + j], paintingBuffer);
		}
	}
}

void Submenu::drawMenu(uint16_t* paintingBuffer) {
	// drawing title
	drawString(name, COLOR_BLACK, xSubmenuOffset, ySubmenuOffset, paintingBuffer, scaleTitle);

	// drawing "walls"
	drawString("Walls:", COLOR_BLACK, xSubmenuOffset, ySubmenuOffset + 2 * ROW_HEIGHT,
		paintingBuffer, scaleWalls);

	//drawing wallsNum
	drawString(wallsNum, COLOR_BLACK, xSubmenuOffset + 160, ySubmenuOffset + 2 * ROW_HEIGHT + 8,
		paintingBuffer, scaleWalls - 1);

	// drawing "ceiling"
	drawString("Ceiling:", COLOR_BLACK, xSubmenuOffset, ySubmenuOffset + 4 * ROW_HEIGHT,
		paintingBuffer, scaleCeiling);

	//drawing ceilingNum - prepare for 3 num
	//xOffset: xSubmenuOffset + 160: default offset of num;  (scaleCeiling-1)*8*3: move 3 letters right
	drawString(ceilingNum, COLOR_BLACK, xSubmenuOffset + 180, ySubmenuOffset + 4 * ROW_HEIGHT + 8,
		paintingBuffer, scaleCeiling - 1);
	
	// maybe redundant due to face of ceilingNum
	/*drawString(ceilingNum, COLOR_BLACK, xSubmenuOffset + 160 + (scaleCeiling - 1) * 8 * 3, ySubmenuOffset + 4 * ROW_HEIGHT + 8,
		paintingBuffer, scaleCeiling - 1);
	drawString(ceilingNum, COLOR_BLACK, xSubmenuOffset + 160 + (scaleCeiling - 1) * 8 * 6, ySubmenuOffset + 4 * ROW_HEIGHT + 8,
		paintingBuffer, scaleCeiling - 1);*/

	// drawing "OffLights"
	drawString("Turn off lights", COLOR_BLACK, xSubmenuOffset, ySubmenuOffset + 6 * ROW_HEIGHT,
		paintingBuffer, scaleOffLight);

	// drawing "OnLights"
	drawString("Turn on lights", COLOR_BLACK, xSubmenuOffset, ySubmenuOffset + 8 * ROW_HEIGHT,
		paintingBuffer, scaleOnLights);
}

bool Submenu::checkReturn() {
	if (knobs->isGreenKnobPressed()) {
		curState = MENU;
		knobs->annulate();
		return true;
	}
	return false;
}

void Submenu::highlightCurRow() {
	int curCurRow = cursor->getCurRow();
	if (curCurRow == 0) scaleWalls = defaultScale + 1;
	else if (curCurRow == 1) scaleCeiling = defaultScale + 1;
	else if (curCurRow == 2) scaleOffLight = defaultScale + 1;
	else if (curCurRow == 3) scaleOnLights = defaultScale + 1;
	else cerr << "Submenu->highlightCurRow: current row of currsor is not 0/1" << endl;
}

string Submenu::toStringRGB888(RGB888_t value) {
	return to_string(value.red) + " " + to_string(value.green) + " " + to_string(value.blue);
}

void Submenu::fillColorsChoice() {
	//for (int i = 0; i < 16; ++i) {
	colorsChoice[0] = COLOR_BLUE;
	colorsChoice[1] = COLOR_RED;
	colorsChoice[2] = COLOR_GREEN;
	colorsChoice[3] = 0xFCE0;
	colorsChoice[4] = 0xFFA0;
	colorsChoice[5] = 0xA7E0;
	colorsChoice[6] = 0x27E0;
	colorsChoice[7] = 0x06D9;
	colorsChoice[8] = 0x04BB;
	colorsChoice[9] = 0x01BB;
	colorsChoice[10] = 0x701B;
	colorsChoice[11] = 0xC81B;
	colorsChoice[12] = 0xD80B;
	colorsChoice[13] = 0xFCB9;
	colorsChoice[14] = 0x72AC;
	colorsChoice[15] = 0x83EB;
}

void Submenu::updateXYPosCursor2() {
	xPosCursor2 = xCursor2Offset + 2 * colPosCursor2*ROW_HEIGHT;
	yPosCursor2 = yCursor2Offset + 2 * rowPosCursor2*ROW_HEIGHT;
}

// GETTERS

int* Submenu::getPMaxRow() {
	return &maxRow;
}

int Submenu::getDefaultScale() {
	return defaultScale;
}

int* Submenu::getPCurMinRow() {
	return &curMinRow;
}

int* Submenu::getPCurMaxRow() {
	return &curMaxRow;
}

int Submenu::getMaxRowInWin() {
	return maxRowInWin;
}

/*if (incrementingWalls) {
		if (knobs->isGreenKnobPressed()) {
			incrementingWalls = false;
			knobs->annulate();
			return;
		}
		if (knobs->isBlueTurnRight() && *walls < 64) ++(*walls);
		else if (knobs->isBlueTurnLeft() && *walls > 0) --(*walls);
	}
	else if (incrementingCeiling) {
		if (knobs->isGreenKnobPressed()) {
			incrementingCeiling = false;
			knobs->annulate();
			return;
		}
		if (knobs->isBlueTurnRight() && *ceiling < 64) ++(*ceiling);
		else if (knobs->isBlueTurnLeft() && *ceiling > 0) --(*ceiling);
	}
	else {
		// returning to menu (by pressing green knob)
		if (knobs->isGreenKnobPressed()) {
			menu->deactiveSubmenu();
			return;
		}
		if (knobs->isBlueKnobPressed()) {
			if (cursor->getCurRow() == 0) {
				incrementingWalls = true;
				knobs->annulate();
				return;
			}
			else if (cursor->getCurRow() == 1) {
				incrementingCeiling = true;
				knobs->annulate();
				return;
			}
			else cerr << "Submenu.update: Strength idx of cursor" << endl;
		}
		else if (knobs->isBlueTurnRight()) cursor->moveUp();
		else if (knobs->isBlueTurnLeft()) cursor->moveDown();
		}*/
