#ifndef __CONSTANT_H__
#define __CONSTANT_H__

#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

#define COLOR_WHITE	0xffff
#define COLOR_BLACK	0x0000
#define COLOR_RED	0xf800
#define COLOR_GREEN	0x07E0
#define COLOR_BLUE	0x001F

typedef struct RGB888 {
	uint8_t red;
	uint8_t green;
	uint8_t blue;
} RGB888_t;

const int WIDTH = 480;
const int HEIGHT = 320;
const int ROW_HEIGHT = 32;
const uint16_t ICON_IMAGE_SIZE = 256;

void drawLetter(char letter, uint16_t color, int offset_x, int offset_y, uint16_t *framebuffer, int scale);

void drawString(string str, uint16_t color, int offset_x, int offset_y, uint16_t *framebuffer, int scale);

uint32_t RGB565toRGB888(uint16_t rgb565color);
uint16_t RGB888toRGB565(uint32_t rgb888color);

uint16_t RGB888toUint16(RGB888_t rgb888color);
RGB888_t uint16toRGB888(uint16_t rgb565color);

uint32_t RGB888_tToUint32_t(RGB888_t rgb888struct);
RGB888_t uint32_tToRGB888_t(uint32_t rgb888color);

//calculates new color value for type 1 messages
uint8_t getNewColorIntensity(int16_t colorIntensity, int16_t intensityChange);

// set buffer to WHITE from given column to right
void cleanPaintingBuffer(uint16_t* paintingBuffer, int col);
void cleanPaintingBuffer(uint16_t* paintingBuffer);

// draw and fill rectangle
void fillRectangle(int xPos, int yPos, int width, uint16_t color, uint16_t* paintingBuffer);

void drawImg(uint16_t* paintingBuffer, uint16_t *img, int width, int height, int xPos, int yPos);

//load img from ppm file (ppm without comments!!)
RGB888_t* loadImg(const char* name);

#endif
