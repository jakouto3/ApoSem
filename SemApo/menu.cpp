#include "menu.h"
#include "unit.h"
#include "cursor.h"
#include "submenu.h"
#include "knobs.h"


Menu::Menu(Knobs* knobs, int xMenuOffset, int yMenuOffset, int maxRowInWin, int defaultScale) {
	this->knobs = knobs;
	knobs->annulate();
	this->xMenuOffset = xMenuOffset;
	this->yMenuOffset = yMenuOffset;
	this->maxRowInWin = maxRowInWin;
	this->defaultScale = defaultScale;
	curMinRow = 0;
	curMaxRow = maxRowInWin - 1;
	submenuActive = false;
	cursor = NULL; // cursor is created with first added unit
	submenu = NULL;
	cerr << "Menu: Menu has been created" << endl;
}

// DESTRUCTOR
Menu::~Menu() {
	for (vector<Unit*>::iterator i = units.begin(); i != units.end(); ++i) {
		delete *i;
	}
	knobs->annulate();
	delete cursor;
	cerr << "Menu: Menu has been deleted" << endl;
}

// SCELET METHODS

void Menu::update() {
	if (submenuActive) {
		submenu->update();
	}else {
		if (cursor != NULL) {
			if (knobs->isBlueTurnRight()) cursor->moveUp();
			else if (knobs->isBlueTurnLeft()) cursor->moveDown();
			else if (knobs->isBlueKnobPressed()) {
				submenuActive = true;
				submenu = new Submenu(knobs, this, units[cursor->getCurRow()]);
			}
			cursor->update();
		}
		for (vector<Unit*>::iterator i = units.begin(); i != units.end(); ++i) {
			(*i)->update();
		}
		highlightCurUnit(); // must be after updating units
		//TODO
	} 
}

void Menu::draw(uint16_t* paintingBuffer) {
	//cerr << "Drawing" << endl;
	if (submenuActive) {
		submenu->draw(paintingBuffer);
	}
	else {
		cleanPaintingBuffer(paintingBuffer);

		// calling draw() method on just current viewed units
		for (int i = curMinRow; i <= curMaxRow; ++i) {
			if (i >= maxRow) {
				//cerr << "Menu: Cannot move cursor down, last unit reached" << endl;
				break;
			}
			units[i]->draw(paintingBuffer);
			//cout << units[i]->toString() << endl;
		}
		cursor->draw(paintingBuffer);
		/*cout << cursor->toString() << endl << endl << endl;
		int cursorRow;
		cin >> cursorRow;
		if (cursorRow == 2) cursor->moveDown();
		else if (cursorRow == 5) cursor->moveUp();*/
	}
}



void Menu::addUnit(Unit* unit) {
	unit->setRow(units.size());
	units.push_back(unit);
	++maxRow;

	//if (curMaxRow - curMinRow + 1 < maxRowInWin) ++curMaxRow;

	if (cursor == NULL) {
		cursor = new Cursor(20, yMenuOffset, this);
	}

	cerr << "Menu: " << (*unit).toString() << " has been added to menu" << endl;
}

void Menu::removeUnit(int idx) {

	if (idx >= maxRow){
		cerr << "Menu.removeUnit: given idx is bigger than number of units" << endl;
		return;
	}
	//delete units[idx];
             
	units.erase(units.begin()+idx);
	int i = 0;
	for (vector<Unit*>::iterator it = units.begin(); it != units.end(); ++it) {
		(*it)->setRow(i);
		(*it)->updateCoorToDrawFromRowPos();
		++i;
	}
	--maxRow;
	cursor->setCurRow(0);
	//if (cursor->getCurRow() >= idx) cursor->moveUp();
}

string Menu::toString() {
	string res = "";

	res += cursor->toString() + "\n";
	for (vector<Unit*>::iterator i = units.begin(); i != units.end(); ++i) {
		res += (*i)->toString();
		res += "\n";
	}
	return res;
}

bool Menu::isEmpty() {
	if (maxRow > 0) return false;
	if (units.size() > 0) return false;
	return true;
}

void Menu::deactiveSubmenu() {
	submenuActive = false;
	delete submenu;
	submenu = NULL;
}

void Menu::highlightCurUnit() {
	int rowToHighlight = cursor->getCurRow();
	units[rowToHighlight]->setScale(defaultScale + 1);
	//cerr << "Highlighted row " << rowToHighlight << endl;
}

// GETTERS

int Menu::getDefaultScale() {
	return defaultScale;
}

int Menu::getXMenuOffset() {
	return xMenuOffset;
}

int Menu::getYMenuOffset() {
	return yMenuOffset;
}

int Menu::getMaxRow() {
	return maxRow;
}

int* Menu::getPMaxRow() {
	return &maxRow;
}

int Menu::getMaxRowInWin() {
	return maxRowInWin;
}

int* Menu::getPCurMinRow() {
	return &curMinRow;
}

int* Menu::getPCurMaxRow() {
	return &curMaxRow;
}

bool Menu::isSubmenuActive() {
	return submenuActive;
}

vector<Unit*>& Menu::getUnitVector() {
	return units;
}
