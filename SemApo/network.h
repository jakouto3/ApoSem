#ifndef __NETWORK_H__
#define __NETWORK_H__

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <thread>
#include <chrono>
#include <cstring>
#include "network_utils.h"
#include "support.h"
#include "unit.h"
#include "menu.h"

using namespace std;

void statusMessageSender(Unit* theUnit);

void sendStatusMessage(Unit* theUnit);

void messageReceiver(Menu* theMenu);

void handleType0Message(char* messageArray, uint32_t ipaddress, Menu* theMenu);

void handleType1Message(char* messageArray, Unit* theUnit);

void handleType2Message(char* messageArray, Unit* theUnit);

void sendType2Message(Unit* theUnit);

#endif

