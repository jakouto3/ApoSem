#ifndef __UNIT_H__
#define __UNIT_H__

#include <iostream>
#include <stdio.h>
#include <string>
#include <atomic>

#include "support.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

using namespace std;

class Menu;

/**
 * Class reprezenting one unit and her properties.
 */
class Unit {
public:
	Unit(string name, RGB888_t ceiling, RGB888_t walls, uint16_t *image, Menu *menu);
	Unit(string name, RGB888_t ceiling, RGB888_t walls, uint16_t *image, Menu *menu, bool local);
	~Unit();

	/**
	 * Repeatedly called method update, 
	 * updating Unit properties, position etc.
	 */
	void update(); // TODO

	/**
	 * Repeatedly called method draw, 
	 * drawing Unit to painting buffer
	 */
	void draw(uint16_t* paintingBuffer); // TODO

	string toString();
	
	void updateCoorToDrawFromRowPos();

	// Setters
	void setName(string name);
	void setWallsCeiling(RGB888_t walls, RGB888_t ceiling);
	void setWalls(RGB888_t walls);
	void setCeiling(RGB888_t Ceiling);
	void setImage(uint16_t *image);
	void setIPAddress(uint32_t ipaddress);
	void setXYPos(int x, int y);
	void setRow(int row);
	void setScale(int scale);
	void setIsInSubmenu(bool inSubmenu);
	void setTOLCounter(uint32_t newCounter);

	// Getters
	char* getBufferName();
	string getName();
	RGB888_t getCeiling();
	RGB888_t getWalls();
	uint16_t* getImage();
	uint32_t getIPAddress();
	int getXPos();
	int getYPos();
	int getRow();
	bool isLocal();
	bool isInSubmenu();
	uint32_t getTOLCounter();
	//RGB888_t* getPWalls();
	//RGB888_t* getPCeiling();

private:
	Menu *menu;
	string name;
	char bufferName[16]; // label/name of unit, max 16 chars, non-used chars set to 0
	atomic<RGB888_t> ceiling; // ceiling RGB 888, highest 8 bits non-used
	atomic<RGB888_t> walls; // walls RGB 888, highest 8 bits non-used
	uint16_t *image; // graphic symbol reprezenting room
	atomic<uint32_t> IPAddress;
	atomic<uint32_t> timeOfLifeCounter;

	int xPos; // x position in display (left, up corner of Unit 'image')
	int yPos; // y position in display (left, up corner of Unit 'image')
	int row; // row to which belongs
	int defaultScale;
	int scale;
	void infoNewUnit();
	
	bool inSubmenu;
	bool local;
};







#endif
