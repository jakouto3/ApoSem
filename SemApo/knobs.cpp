#include "knobs.h"

Knobs::Knobs() {
	knobMemBase = (unsigned char*)map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);

	if (knobMemBase == NULL) {
		cerr << "Knobs: Error: knob_mem_base == NULL" << endl;
		exit(1);
	}
	rgbKnobsValue = *(volatile uint32_t*)(knobMemBase + SPILED_REG_KNOBS_8BIT_o);
	blueCurVal = bluePrevVal = (rgbKnobsValue & 0x000000FF);
	greenCurVal = greenPrevVal = (rgbKnobsValue & 0x0000FF00)>>8;
	redCurVal = redPrevVal = (rgbKnobsValue & 0x00FF0000)>>16;

	bluePressed = greenPressed = redPressed = false;

	cerr << "Knobs: Knobs have been created" << endl;
}

Knobs::~Knobs() {
	cerr << "Knobs: Knobs have been deleted" << endl;
}

void Knobs::update() {
	rgbKnobsValue = *(volatile uint32_t*)(knobMemBase + SPILED_REG_KNOBS_8BIT_o);
	blueCurVal = rgbKnobsValue & 0x000000FF;
	greenCurVal = (rgbKnobsValue & 0x0000FF00)>>8;
	redCurVal = (rgbKnobsValue & 0x00FF0000)>>16;
	pressedVal = (rgbKnobsValue & 0x07000000)>>24;

	// BLUE KNOB TURNING
	blueKnobChange = blueCurVal - bluePrevVal;
	if (blueKnobChange < -100) blueKnobChange = blueKnobChange + 255 + 1; // overflow of unsigned char
	else if (blueKnobChange > 100) blueKnobChange = blueKnobChange - 255 - 1; // underflow of unsigned char

	// GREEN KNOB TURNING
	greenKnobChange = greenCurVal - greenPrevVal;
	if (greenKnobChange < -100) greenKnobChange = greenKnobChange + 255 + 1; // overflow of unsigned char
	else if (greenKnobChange > 100) greenKnobChange = greenKnobChange - 255 - 1; // underflow of unsigned char

	// RED KNOB TURNING
	redKnobChange = redCurVal - redPrevVal;
	if (redKnobChange < -100) redKnobChange = redKnobChange + 255 + 1; // overflow of unsigned char
	else if (redKnobChange > 100) redKnobChange = redKnobChange - 255 - 1; // underflow of unsigned char

	// ANY KNOB PRESSED
	if (pressedVal == 0) {}
	else if (pressedVal == 1)  bluePressed = true;
	else if (pressedVal == 2)  greenPressed = true;
	else if (pressedVal == 4)  redPressed = true;
	else cerr << "Knobs.update: knob pressed loading state error..." << endl;
}

void Knobs::annulate() {
	bluePrevVal = blueCurVal;
	greenPrevVal = greenCurVal;
	redPrevVal = redCurVal;

	blueKnobChange = redKnobChange = greenKnobChange = 0;
	bluePressed = greenPressed = redPressed = false;
	cerr << "Knobs: knobs have been annulated" << endl;
}


// GETTERS

int Knobs::getBlueKnobChange() {
	bluePrevVal = blueCurVal;
	return blueKnobChange;
}

int Knobs::getGreenKnobChange() {
	greenPrevVal = greenCurVal;
	return greenKnobChange;
}

int Knobs::getRedKnobChange() {
	redPrevVal = redCurVal;
	return redKnobChange;
}

bool Knobs::isBlueTurnRight() {
	bluePrevVal = blueCurVal;
	if (blueKnobChange > 0) return true;
	return false;
}

bool Knobs::isBlueTurnLeft() {
	bluePrevVal = blueCurVal;
	if (blueKnobChange < 0) return true;
	return false;
}

bool Knobs::isGreenTurnRight() {
	greenPrevVal = greenCurVal;
	if (greenKnobChange > 0) return true;
	return false;
}

bool Knobs::isGreenTurnLeft() {
	greenPrevVal = greenCurVal;
	if (greenKnobChange < 0) return true;
	return false;
}

bool Knobs::isRedTurnRight() {
	redPrevVal = redCurVal;
	if (redKnobChange > 0) return true;
	return false;
}

bool Knobs::isRedTurnLeft() {
	redPrevVal = redCurVal;
	if (redKnobChange < 0) return true;
	return false;
}

bool Knobs::isBlueKnobPressed() {
	bool isPressed = bluePressed;
	bluePressed = false;
	return isPressed;
}

bool Knobs::isGreenKnobPressed() {
	bool isPressed = greenPressed;
	greenPressed = false;
	return isPressed;
}

bool Knobs::isRedKnobPressed() {
	bool isPressed = redPressed;
	redPressed = false;
	return isPressed;
}
