#ifndef __MENU_H__
#define __MENU_H__

#include <iostream>
#include <vector>
#include <string>

#include "support.h"

using namespace std;



class Unit;
class Cursor;
class Submenu;
class Knobs;
/**
 *  
 * removing unit not implemented
 */
class Menu {
public:
	Menu(Knobs* knobs, int xMenuOffset, int yMenuOffset, int maxRowInWin, int defaultScale);
	~Menu();

	/**
	* Repeatedly called method update,
	* updating menu: calling cursor, units update() method
	*/
	void update(); // TODO reacting to knobs

	/**
	* Repeatedly called method draw,
	* clean buffer and calling cursor, units draw() method
	*/
	void draw(uint16_t* paintingBuffer);

	/**
	* Add Unit to lists (vector) of units
	*/
	void addUnit(Unit* unit);
	void removeUnit(int idx);//INCOMPLETE
	string toString();
	bool isEmpty();

	void deactiveSubmenu();

	// GETTERS
	int getDefaultScale();
	int getXMenuOffset();
	int getYMenuOffset();
	int getMaxRow();
	int* getPMaxRow(); // return pointer to maxRow
	int getMaxRowInWin();
	int* getPCurMinRow();
	int* getPCurMaxRow();
	bool isSubmenuActive();
	vector<Unit*>& getUnitVector();
	
	
private:
	Knobs* knobs;
	Cursor *cursor; // cursor is created with first added unit
	Submenu *submenu;
	vector<Unit*> units;
	int xMenuOffset; // x offset of menu in window
	int yMenuOffset; // y offset of menu in window
	
	int curMinRow; // current number of min viewed row of Unit
	int curMaxRow; // current number of max viewed row of Unit
	int maxRow; // number of all rows (equal to number of units)

	int maxRowInWin; // maximum of rows, which can be viewed in window
	int defaultScale; // default scale of all strings
	
	void highlightCurUnit(); // highlight line with unit on line of cursor

	bool submenuActive;

};




#endif
