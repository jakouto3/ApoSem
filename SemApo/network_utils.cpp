#include "network_utils.h"

void createMessageHeader(char* messageArray, uint32_t messageType) {
	uint32_t magicNumberNetworkOrder = htonl(MAGIC_NUMBER_ALC1);
	uint32_t protocolVersionNetworkOrder = htonl(ALC_PROTOCOL_VERSION);
	uint32_t messageTypeNetworkOrder = htonl(messageType);

	memcpy(messageArray, &magicNumberNetworkOrder, sizeof(uint32_t));
	memcpy(messageArray + sizeof(uint32_t), &protocolVersionNetworkOrder, sizeof(uint32_t));
	memcpy(messageArray + 2 * sizeof(uint32_t), &messageTypeNetworkOrder, sizeof(uint32_t));
}

void createStatusMessage(char* messageArray, Unit* theUnit) {
	createMessageHeader(messageArray, MESSAGE_TYPE_STATUS);
	uint32_t ceilingColorNetworkOrder = htonl(RGB888_tToUint32_t(theUnit->getCeiling()));
	uint32_t wallColorNetworkOrder = htonl(RGB888_tToUint32_t(theUnit->getWalls()));	
	string unitName = theUnit->getName();
	char unitNameBuff[16] = {0};
	memcpy(unitNameBuff, unitName.c_str(), unitName.length());

	memcpy(messageArray + MESSAGE_HEADER_SIZE, &ceilingColorNetworkOrder, sizeof(uint32_t));
	memcpy(messageArray + MESSAGE_HEADER_SIZE + sizeof(uint32_t), &wallColorNetworkOrder, sizeof(uint32_t));
	memcpy(messageArray + MESSAGE_HEADER_SIZE + 2 * sizeof(uint32_t), unitNameBuff, sizeof(unitNameBuff));
	for (uint16_t iconPxIdx = 0; iconPxIdx < ICON_IMAGE_SIZE; ++iconPxIdx)
	{
		uint16_t iconPxNetworkOrder = htons(theUnit->getImage()[iconPxIdx]);
		uint32_t offsetInMessagePacket = MESSAGE_HEADER_SIZE + 2 * sizeof(uint32_t) + sizeof(unitNameBuff) + iconPxIdx * sizeof(uint16_t);
		memcpy(messageArray + offsetInMessagePacket, &iconPxNetworkOrder, sizeof(uint16_t));
	}

}

uint32_t getMessageType(char* messageArray) {
	uint32_t messageType;
	memcpy(&messageType, messageArray + 2 * sizeof(uint32_t), sizeof(uint32_t));
	return ntohl(messageType);
}

uint16_t* getIconFromMessage(char* messageArray) {
	uint16_t* icon = new uint16_t[ICON_IMAGE_SIZE];
	for (uint16_t iconIdx = 0; iconIdx < ICON_IMAGE_SIZE; ++iconIdx) {
		uint16_t onePxNetworkOrder;
		uint32_t offsetInMessage = MESSAGE_HEADER_SIZE + 2 * sizeof(uint32_t) + 16 * sizeof(char) + iconIdx * sizeof(uint16_t);
		memcpy(&onePxNetworkOrder, messageArray + offsetInMessage, sizeof(uint16_t));
		icon[iconIdx] = ntohs(onePxNetworkOrder);
	}
	return icon;
}

bool isReceivedMessageValidALCMessage(char* messageArray) {
	bool messageValid = false;
	uint32_t receivedMagicNumber, receivedProtocolVersion;
	memcpy(&receivedMagicNumber, messageArray, sizeof(uint32_t));
	memcpy(&receivedProtocolVersion, messageArray + sizeof(uint32_t), sizeof(uint32_t));

	receivedMagicNumber = ntohl(receivedMagicNumber);
	receivedProtocolVersion = ntohl(receivedProtocolVersion);
	if (receivedMagicNumber == MAGIC_NUMBER_ALC1 && receivedProtocolVersion == ALC_PROTOCOL_VERSION) {
		messageValid = true;
	}
	return messageValid;
}

Unit* createNewUnitFromMessage(char* messageArray, uint32_t ipaddress, Menu* theMenu) {
	uint32_t ceilingColor, wallColor;
	char nameBuffer[16] = { 0 };
	string name;

	memcpy(&ceilingColor, messageArray + MESSAGE_HEADER_SIZE, sizeof(uint32_t));
	memcpy(&wallColor, messageArray + MESSAGE_HEADER_SIZE + sizeof(uint32_t), sizeof(uint32_t));
	ceilingColor = ntohl(ceilingColor); //transform from network order to host order
	wallColor = ntohl(wallColor);

	memcpy(nameBuffer, messageArray + MESSAGE_HEADER_SIZE + 2 * sizeof(uint32_t), sizeof(nameBuffer));
	name.assign(nameBuffer, 0, sizeof(nameBuffer));

	uint16_t* image = getIconFromMessage(messageArray);
	Unit* newUnit = new Unit(name, uint32_tToRGB888_t(ceilingColor), uint32_tToRGB888_t(wallColor), image, theMenu, false);
	
	newUnit->setIPAddress(ipaddress);
	newUnit->setTOLCounter(theMenu->getUnitVector()[0]->getTOLCounter());

	return newUnit;
}

void updateUnitFromStatusMessage(char* messageArray, Unit* theUnit) {
	if(!theUnit->isInSubmenu()){
		uint32_t ceilingColor, wallColor;

		memcpy(&ceilingColor, messageArray + MESSAGE_HEADER_SIZE, sizeof(uint32_t));
		memcpy(&wallColor, messageArray + MESSAGE_HEADER_SIZE + sizeof(uint32_t), sizeof(uint32_t));
		ceilingColor = ntohl(ceilingColor); //transform from network order to host order
		wallColor = ntohl(wallColor);

		theUnit->setWallsCeiling(uint32_tToRGB888_t(wallColor), uint32_tToRGB888_t(ceilingColor));
		theUnit->setTOLCounter(theUnit->getTOLCounter()+1);
	}
}

string getNameFromStatusMessage(char* messageArray) {
	char nameBuffer[16] = { 0 };
	string name;
	memcpy(nameBuffer, messageArray + MESSAGE_HEADER_SIZE + 2 * sizeof(uint32_t), sizeof(nameBuffer));
	name.assign(nameBuffer, 0, sizeof(nameBuffer));
	return name;
}

void createType2Message(char* messageArray, Unit* theUnit) {
	int16_t ceilingRed, ceilingGreen, ceilingBlue, wallsRed, wallsGreen, wallsBlue;
	RGB888_t ceilingColor = theUnit->getCeiling();
	RGB888_t wallsColor = theUnit->getWalls();
	
	createMessageHeader(messageArray, MESSAGE_TYPE_SET);
	
	ceilingRed = htons((int16_t)ceilingColor.red);
	ceilingGreen = htons((int16_t)ceilingColor.green);
	ceilingBlue = htons((int16_t)ceilingColor.blue);
	
	wallsRed = htons((int16_t)wallsColor.red);
	wallsGreen = htons((int16_t)wallsColor.green);
	wallsBlue = htons((int16_t)wallsColor.blue);
	
	memcpy(messageArray + MESSAGE_HEADER_SIZE, &ceilingRed, sizeof(int16_t));
	memcpy(messageArray + MESSAGE_HEADER_SIZE + sizeof(int16_t), &ceilingGreen, sizeof(int16_t));
	memcpy(messageArray + MESSAGE_HEADER_SIZE + 2 * sizeof(int16_t), &ceilingBlue, sizeof(int16_t));
	
	memcpy(messageArray + MESSAGE_HEADER_SIZE + 3 * sizeof(int16_t), &wallsRed, sizeof(int16_t));
	memcpy(messageArray + MESSAGE_HEADER_SIZE + 4 * sizeof(int16_t), &wallsGreen, sizeof(int16_t));
	memcpy(messageArray + MESSAGE_HEADER_SIZE + 5 * sizeof(int16_t), &wallsBlue, sizeof(int16_t));	
}

