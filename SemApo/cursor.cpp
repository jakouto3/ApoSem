#include "cursor.h"
#include "menu.h"
#include "submenu.h"

// for MENU
Cursor::Cursor(int xCursorOffset, int yCursorOffset, Menu* menu) {
	this->xCursorOffset = xCursorOffset;
	this->yCursorOffset = yCursorOffset + 5;
	this->maxRowInWin = menu->getMaxRowInWin();
	this->curRow = 0;
	this->maxRow = menu->getPMaxRow();
	this->scale = menu->getDefaultScale();
	curMinRow = menu->getPCurMinRow();
	curMaxRow = menu->getPCurMaxRow();

	xPos = xCursorOffset;
	yPos = yCursorOffset;

	cerr << "Cursor: Cursor for menu was created" << endl;
}

// for SUBMENU
Cursor::Cursor(int xCursorOffset, int yCursorOffset, Submenu* menu) {
	this->xCursorOffset = xCursorOffset;
	this->yCursorOffset = yCursorOffset + 5;
	this->maxRowInWin = menu->getMaxRowInWin();
	this->curRow = 0;
	this->maxRow = menu->getPMaxRow();
	this->scale = menu->getDefaultScale();
	curMinRow = menu->getPCurMinRow();
	curMaxRow = menu->getPCurMaxRow();

	xPos = xCursorOffset;
	yPos = yCursorOffset;

	cerr << "Cursor: Cursor for submenu was created" << endl;
}

Cursor::~Cursor() {
	cerr << "Cursor: Cursor was deleted" << endl;
}

void Cursor::update() {
	// xPos = xCursorOffset; xPos cursor is same
	// number 2 in next calculation always skip one row
	yPos = yCursorOffset + ((2 * curRow) % (2 * maxRowInWin))*ROW_HEIGHT;
}

void Cursor::draw(uint16_t* paintingBuffer) {
	drawLetter(0x10, COLOR_BLACK, xPos, yPos, paintingBuffer, scale);
}

string Cursor::toString() {
	return "Cursor: row: " + to_string(curRow) + 
		" [x,y] = [" + to_string(xPos) + "][" + to_string(yPos) + "]";
}

void Cursor::moveDown() {
	if (curRow >= *maxRow - 1) {
		cerr << "Cursor: cannot move cursor down" << endl;
		return;
	}
	++curRow;
	if (*curMaxRow < curRow) {
		(*curMaxRow) += maxRowInWin;
		(*curMinRow) += maxRowInWin;
	}
}

void Cursor::moveUp() {
	if (curRow <= 0) {
		cerr << "Cursor: cannot move cursor up" << endl;
		return;
	}
	--curRow;
	if (*curMinRow > curRow) {
		(*curMaxRow) -= maxRowInWin;
		(*curMinRow) -= maxRowInWin;
	}
}

// SETTERS
/*
// WARNING - maybe failure in calculating yPos
void Cursor::setCurRow(int curRow) {
	if (curRow > *maxRow) {
		cerr << "Cursor::setCurRow(..): Given curRow is bigger than maxRow" << endl;
		return;
	}
	this->curRow = curRow;
	yPos = yUpCursorOffset + (curRow * 2 * ROW_HEIGHT) % (HEIGHT - yUpCursorOffset - yDownCursorOffset);
}*/

void Cursor::setMaxRow(int* maxRow) {
	this->maxRow = maxRow;
}

void Cursor::increaseMaxRow(int i) {
	(*maxRow) += i;
}

void Cursor::setCurRow(int idx){
		curRow = idx;
	}

// GETTERS

int Cursor::getCurRow() {
	return curRow;
}
