#ifndef __KNOBS_H__
#define __KNOBS_H__

#include <iostream>

#include "mzapo_phys.h"
#include "mzapo_regs.h"

using namespace std;


class Knobs {
public:
	Knobs();
	~Knobs();

	void update();

	// annulate whole class, should be called every time changing state of program
	void annulate();

	// with calling these methods is changed status of this class!!!
	int getBlueKnobChange();
	int getGreenKnobChange();
	int getRedKnobChange();

	// with calling these methods is changed status of this class!!!
	bool isBlueTurnRight();
	bool isBlueTurnLeft();
	bool isGreenTurnRight();
	bool isGreenTurnLeft();
	bool isRedTurnRight();
	bool isRedTurnLeft();
	bool isBlueKnobPressed();
	bool isGreenKnobPressed();
	bool isRedKnobPressed();



private:
	uint32_t rgbKnobsValue;
	uint32_t blueCurVal;
	uint32_t redCurVal;
	uint32_t greenCurVal;
	uint32_t pressedVal;

	uint32_t bluePrevVal;
	uint32_t greenPrevVal;
	uint32_t redPrevVal;

	bool bluePressed;
	bool greenPressed;
	bool redPressed;

	// number about wchich value of knobs was changed since last review
	int blueKnobChange;
	int redKnobChange;
	int greenKnobChange;
	unsigned char* knobMemBase;



};



#endif