#include "support.h"
#include "font_types.h"

void drawLetter(char letter, uint16_t color, int offset_x, int offset_y, uint16_t *framebuffer, int scale) {
	uint32_t letter_idx = (letter) * 16;

	for (int y = 0; y < 16 * scale; y += scale) {
		uint16_t currentRow = font_rom8x16.bits[letter_idx];
		for (int x = 0; x < 8 * scale; x += scale) {
			if (currentRow & 0x8000) {
				uint32_t index;
				for (int u = 0; u < scale; ++u) {
					for (int v = 0; v < scale; ++v) {
						index = (y + offset_y + u) * WIDTH + (x + offset_x + v);
						framebuffer[index] = color;
					}
				}
			}
			else {
				//color = COLOR_WHITE;
			}
			currentRow <<= 1;
		}
		++letter_idx;
	}
}

void drawString(string str, uint16_t color, int offset_x, int offset_y, uint16_t *framebuffer, int scale) {
	int widthOfSpace = 8;
	for (unsigned int i = 0; i < str.length(); ++i) {
		drawLetter(str[i], color, offset_x + i*widthOfSpace*scale, offset_y, framebuffer, scale);
	}
}

uint32_t RGB565toRGB888(uint16_t rgb565color){
	uint8_t red = ((rgb565color >> 11) & 0x1F) << 3;
	uint8_t green = ((rgb565color >> 5) & 0x3F) << 2;
	uint8_t blue = ((rgb565color) & 0x1F) << 3;
	uint32_t rgb888color = 0x0 | ((red << 16) | (green << 8) | (blue));
	return rgb888color;
}

uint16_t RGB888toRGB565(uint32_t rgb888color) {
	uint8_t red = ((rgb888color >> 16) & 0xFF) >> 3;
	uint8_t green = ((rgb888color >> 8) & 0xFF) >> 2;
	uint8_t blue = ((rgb888color) & 0xFF) >> 3;
	uint16_t rgb565color = 0x0 | (red << 11) | (green << 5) | blue;
	return rgb565color;
}

uint16_t RGB888toUint16(RGB888_t rgb888struct) {
	uint32_t rgb888color = RGB888_tToUint32_t(rgb888struct);
	return RGB888toRGB565(rgb888color);
}

RGB888_t uint16toRGB888(uint16_t rgb565color) {
	uint32_t rgb888color = RGB565toRGB888(rgb565color);
	return uint32_tToRGB888_t(rgb888color);
}

uint32_t RGB888_tToUint32_t(RGB888_t rgb888struct) {
	uint32_t rgb888color = 0x0 | (rgb888struct.red << 16) | (rgb888struct.green << 8) | (rgb888struct.blue);
	return rgb888color;
}

RGB888_t uint32_tToRGB888_t(uint32_t rgb888color) {
	RGB888_t rgb888struct;
	rgb888struct.red = (rgb888color >> 16) & 0xFF;
	rgb888struct.green = (rgb888color >> 8) & 0xFF;
	rgb888struct.blue = rgb888color & 0xFF;
	return rgb888struct;
}

uint8_t getNewColorIntensity(int16_t colorIntensity, int16_t intensityChange) {
	int16_t newIntensity = colorIntensity + intensityChange;
	if (newIntensity > 255) newIntensity = 255;
	if (newIntensity < 0) newIntensity = 0;
	return (uint8_t) newIntensity;
}

void cleanPaintingBuffer(uint16_t* paintingBuffer, int col) {
	for (int i = 0; i < HEIGHT; i++) {
		for (int j = 0; j < WIDTH; j++) {
			if (j < col) continue;
			uint32_t index = i * WIDTH + j;
			paintingBuffer[index] = COLOR_WHITE;
		}
	}
}

void cleanPaintingBuffer(uint16_t* paintingBuffer) {
	cleanPaintingBuffer(paintingBuffer, 0);
}

void fillRectangle(int xPos, int yPos, int width, uint16_t color, uint16_t* paintingBuffer) {
	for (int i = yPos; i < yPos + width; i++) {
		for (int j = xPos; j < xPos + width; j++) {
			uint32_t index = i * WIDTH + j;
			paintingBuffer[index] = color;
		}
	}
}

void drawImg(uint16_t* paintingBuffer, uint16_t *img, int width, int height, int xPos, int yPos) {
	for (int i = yPos, k = 0; i < yPos + height; i++, k++) {
		for (int j = xPos, l = 0; j < xPos + width; j++, l++) {
			uint32_t index = i * WIDTH + j;
			uint32_t indexImg = k * width + l;
			paintingBuffer[index] = img[indexImg];
		}
	}
}

//LOCAL FOR LOADING PPM
typedef struct {
	int width, height;
	RGB888_t *data;
} ppm_t;

//LOCAL FOR LOADING IMG
ppm_t *readPPM(const char *file)
{
	char begin[20];
	ppm_t *img;
	FILE *f;
	//int c;
	int max_intensity;

	f = fopen(file, "r");
	if (!f) {
		fprintf(stderr, "Error: cannot open file\n");
		return NULL;
	}

	//read image format
	if (!fgets(begin, sizeof(begin), f)) {
		fprintf(stderr, "Error: loading beginning of file\n");;
		return NULL;
	}

	img = (ppm_t *)malloc(sizeof(ppm_t));
	if (!img) {
		fprintf(stderr, "Error: allocation failed\n");
		return NULL;
	}

	if (fscanf(f, "%d %d", &img->width, &img->height) != 2) {
		fprintf(stderr, "Error: incorrect loading of width/height\n");
		return NULL;
	} // img size
	if (fscanf(f, "%d", &max_intensity) != 1) {
		fprintf(stderr, "Error: incorrect loading of max intensity\n");
		return NULL;
	} // max intensity

	while (fgetc(f) != '\n');
	//memory allocation for pixel data
	img->data = (RGB888_t*)malloc(img->width * img->height * sizeof(RGB888_t));

	if (!img->data) {
		fprintf(stderr, "Error: allocation failed\n");
		return NULL;
	}

	//read pixel data from file
	if (fread(img->data, 3 * img->width, img->height, f) != (unsigned int)img->height) {
		fprintf(stderr, "Error: loading image\n");
		return NULL;
	}

	fclose(f);
	return img;
}

RGB888_t* loadImg(const char* name) {
	ppm_t *image;
	image = readPPM(name);
	RGB888_t* img = image->data;
	free(image);
	return img;
}
