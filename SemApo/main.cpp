

#define _POSIX_C_SOURCE 200112L

#include <iostream>
#include <thread>

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"

#include "unit.h"
#include "menu.h"
#include "test.h"
#include "knobs.h"
#include "network.h"
using namespace std;

/**
 * TODO
 * 
 * Review conversion function in support.*
 * 
 * 
 * 
 * 
 * 
 * 
 * */




unsigned char* getParlcdMemBase();
void fillMenu(Menu* menu);
void paintToScreen(unsigned char* parlcd_mem_base, uint16_t* paintingBuffer);

int main() {
	//test();

	unsigned char *parlcd_mem_base = getParlcdMemBase();
	uint16_t* paintingBuffer = new uint16_t[WIDTH * HEIGHT];
	Knobs *knobs = new Knobs();
	Menu *menu = new Menu(knobs, 40, 20, 5, 2);
	fillMenu(menu);

	const vector<Unit*>& UnitVector = menu->getUnitVector();
	Unit* thisUnit = UnitVector[0];
	
	thread statusSenderThread(statusMessageSender, thisUnit);

	thread receiverThread(messageReceiver, menu);

	while (true) {
		knobs->update();
		menu->update();
		menu->draw(paintingBuffer);
		paintToScreen(parlcd_mem_base, paintingBuffer);
		/*
		if(knobs->isRedKnobPressed()){
			menu->removeUnit(3);
			}*/
		
		parlcd_delay(50);
	}
	delete paintingBuffer;
	delete menu;
	delete knobs;
	return 0;
}

unsigned char* getParlcdMemBase() {
	unsigned char *parlcd_mem_base;
	parlcd_mem_base = (unsigned char *)map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
	if (parlcd_mem_base == NULL) {
		cerr << "Error: parlcd_mem_base == NULL" << endl;
		exit(1);
	}
	parlcd_hx8357_init(parlcd_mem_base);
	return parlcd_mem_base;
}


void fillMenu(Menu* menu) {
	RGB888_t ceiling;
	RGB888_t walls;
	ceiling.red = ceiling.green = ceiling.blue = 0;
	walls.red = walls.green = walls.blue = 0;

	uint16_t *image = new uint16_t[256];
	for (int i = 0; i < 256; ++i) image[i] = COLOR_BLUE;

	uint16_t *unit01Icon = new uint16_t[256];
	RGB888_t *tmp = loadImg("figure.ppm");
	for (int i = 0; i < 256; ++i) {
		unit01Icon[i] = RGB888toUint16(tmp[i]);
	}
	free(tmp);

	menu->addUnit(new Unit("Master's room", ceiling, walls, unit01Icon, menu, true));
	menu->addUnit(new Unit("Unit02", ceiling, walls, image, menu, true));
	menu->addUnit(new Unit("Unit03", ceiling, walls, image, menu, true));
	menu->addUnit(new Unit("Unit04", ceiling, walls, image, menu, true));
	/*menu->addUnit(new Unit("Unit05", ceiling, walls, image, menu, true));
	menu->addUnit(new Unit("Unit06", ceiling, walls, image, menu, true));
	menu->addUnit(new Unit("Unit07", ceiling, walls, image, menu, true));
	menu->addUnit(new Unit("Unit08", ceiling, walls, image, menu, true));
	menu->addUnit(new Unit("Unit09", ceiling, walls, image, menu, true));
	menu->addUnit(new Unit("Unit10", ceiling, walls, image, menu, true));
	menu->addUnit(new Unit("Unit11", ceiling, walls, image, menu, true));
	menu->addUnit(new Unit("Unit12", ceiling, walls, image, menu, true));
	menu->addUnit(new Unit("Unit13", ceiling, walls, image, menu, true));*/
}

void paintToScreen(unsigned char* parlcd_mem_base, uint16_t* paintingBuffer) {
	parlcd_write_cmd(parlcd_mem_base, 0x2c);
	for (int i = 0; i < 320; i++) {
		for (int j = 0; j < 480; j++) {
			parlcd_write_data(parlcd_mem_base, paintingBuffer[i * WIDTH + j]);
		}
	}
}
